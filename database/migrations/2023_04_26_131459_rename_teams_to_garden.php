<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
         Schema::rename('teams', 'garden');

         Schema::table('garden', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
         });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('garden', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
         });

         Schema::rename('garden', 'teams');

         Schema::table('teams', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
         });
    }
};
